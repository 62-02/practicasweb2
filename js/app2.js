const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click',function(){
    //obtener los datos de los input
    let caloriasM = 0;
    let caloriasF = 0;
    let valorAltura = document.getElementById('valorAltura').value;
    let valorPeso = document.getElementById('valorPeso').value;
    let valorEdad = document.getElementById('valorEdad').value
    //hacer los calculos
    let CalculoIMC = valorPeso/(valorAltura*valorAltura);
    //mostrar los datos
    if(valorEdad > 9 && valorEdad < 18){
        caloriasM= 17.686*valorPeso+658.2;
        caloriasF= 13.384*valorPeso+692.6;
    }
    if(valorEdad > 18 && valorEdad < 31){
        caloriasM= 15.057*valorPeso+692.2;
        caloriasF= 14.818*valorPeso+486.6;
    }
    if(valorEdad > 30 && valorEdad < 61){
        caloriasM= 11.472*valorPeso+873.1;
        caloriasF= 8.126*valorPeso+845.6;
    }
    if(valorEdad >= 60){
        caloriasM= 11.711*valorPeso+587.7;
        caloriasF= 9.082*valorPeso+658.5;
    }

    if(CalculoIMC < 18.5){
        document.getElementById('imgImc').src="/img/01.png"
    }
    if(CalculoIMC >= 18.5 && CalculoIMC < 25){
        document.getElementById('imgImc').src="/img/02.png"
    }
    if(CalculoIMC >= 25 && CalculoIMC < 30){
        document.getElementById('imgImc').src="/img/03.png"
    }
    if(CalculoIMC >= 30 && CalculoIMC < 35){
        document.getElementById('imgImc').src="/img/04.png"
    }
    if(CalculoIMC >= 35 && CalculoIMC < 40){
        document.getElementById('imgImc').src="/img/05.png"
    }
    if(CalculoIMC >= 40){
        document.getElementById('imgImc').src="/img/06.png"
    }
    document.getElementById('caloriasM').value = caloriasM.toFixed(2);
    document.getElementById('caloriasF').value = caloriasF.toFixed(2);
    document.getElementById('IMC').value = CalculoIMC.toFixed(2);
})