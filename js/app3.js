//Declara objeto alumno

let alumno = [{
  matricula: "2015030311",
  nombre: "Reyes Lizarraga Jonathan Alexis",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/wero.jpeg" alt="">',
},
{
  matricula: "2020030321",
  nombre: "Ontiveros Govea Yair Alejandro",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Godvea.jpeg" alt="">',
},
{
  matricula: "2021030262",
  nombre: "Qui Mora Ángel Ernesto",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/GOOOOOOOOOOOOOOOOOOO.jfif" alt="">',
},
{
  matricula: "2021030314",
  nombre: "Peñaloza Pizarro Felipe Andrés",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Elpeña.jfif" alt="">',
},
{
  matricula: "2021030136",
  nombre: "Oscar Alejandro Solis Velarde",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Oscar.jfif" alt="">',
},
{
  matricula: "2019030880",
  nombre: "Quezada Ramos Julio Emiliano",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Eljulio.jpeg" alt="">',
},
{
  matricula: "2021030108",
  nombre: "Fabio Manuel Noriega Fitch",
  grupo: "TI-72",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Flabio.jpeg" alt="">',
},
{
  matricula: "2021030142",
  nombre: "Garcia Gonzales Jorge Enrique",
  grupo: "TI-73",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Postu.jpeg" alt="">',
},
{
  matricula: "2021030115",
  nombre: "Abel Sanchez Urrea",
  grupo: "TI-72",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Abel.jfif" alt="">',
},
{
  matricula: "2021030178",
  nombre: "Juan Jose Tirado Romero",
  grupo: "TI-72",
  carrera: "Tecnologias de la Informacio",
  foto: '<img src="/img/Juanjo.jpeg" alt="">',
}];

let tabla = document.getElementById('tabla')
    for(let i=0;i<alumno.length;i++){
        let fila = tabla.insertRow();

        let Matricula = fila.insertCell(0);
        let Nombre = fila.insertCell(1);
        let Grupo = fila.insertCell(2);
        let Carrera = fila.insertCell(3);
        let Foto = fila.insertCell(4);

        Matricula.innerHTML= alumno[i].matricula;
        Nombre.innerHTML= alumno[i].nombre;
        Grupo.innerHTML= alumno[i].grupo;
        Carrera.innerHTML= alumno[i].carrera;
        Foto.innerHTML= alumno[i].foto;

    }

console.log("Matricula :" + alumno.matricula);
console.log("Nombre :" + alumno.nombre);

alumno.nombre = "Acosta Jose Miguel";
console.log("Nuevo Nombre :" + alumno.nombre);

//Objetos compuestos

let cuentaBanco = {
  numero: "10001",
  banco: "Banorte",
  cliente: {
    nombre: "Jose Lopez",
    fechaNac: "2020-01-01",
    sexo: "M",
  },
  saldo: "10000",
};

console.log("Nombre :" + cuentaBanco.cliente.nombre);
console.log("Saldo :" + cuentaBanco.saldo);
cuentaBanco.cliente.sexo = "F";
console.log(cuentaBanco.cliente.sexo);

//arreglo de productos

let productos = [
  {
    codigo: "1001",
    descripcion: "Atun",
    precio: "34",
  },
  {
    codigo: "1002",
    descripcion: "Jabon de polvo",
    precio: "23",
  },
  {
    codigo: "1003",
    descripcion: "Harina",
    precio: "43",
  },
  {
    codigo: "1004",
    descripcion: "Pasta dental",
    precio: "78",
  },
];

//Mostrar un atributo de un objeto del arreglo
console.log("La descrpcion es"+productos[0].descripcion);

for(let i=0;i<productos.length;i++){
    console.log("Codigo: " +productos[i].codigo);
    console.log("Descripcion: " +productos[i].descripcion);
    console.log("Precio: " +productos[i].precio);
}
