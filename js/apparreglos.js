/* Manejo de arrays */
//Declaración de array con elementos enteros
let arreglo = [4,89,30,10,34,89,10,5,8,28];
let arreglo2 = [0,0,0,0,0,0,0,0,0,0];
//Diseñar una función que recibe como argumento un arreglo de enteros e imprime cada elemento y el tamaño del arreglo

function mostrarArray(arreglo){
    let tamaño = arreglo.length;

    for(let con=0; con < tamaño; ++ con){
        console.log(con + ":" + arreglo[con]);
    }

    console.log("Tamaño :" + tamaño);
}
console.log(mostrarArray(arreglo));

//Función para mostrar el promedio de los elementos de array
function mostrarProm(arreglo){
    let res = 0;
    for(let i = 0; i < arreglo.length; i++){
        res += arreglo[i];
    }
    
    console.log("Promedio : " + (res/arreglo.length));
}
console.log(mostrarProm(arreglo));
//Función para mostrar los valores pares de un arreglo
function mostrarPares(arreglo){
    let tamaño = arreglo.length;

    for(let con=0; con < tamaño; con++){
        if(arreglo[con]%2 == 0){
            console.log(con + ":" + arreglo[con]);
        }
     
    }
}
console.log(mostrarPares(arreglo));
//Función para mostrar el valor mayor de los elementos de un arreglo
function mostrarMayor(arreglo){
    let tamaño = arreglo.length;
    let mayor = 0;
    for(let i = 0; i < tamaño; i++){
        if(arreglo[i] >= arreglo[i+1]){
            arreglo[i+1] = arreglo[i];
            mayor = arreglo[i+1];
        }
    }
    console.log("Mayor: " + mayor);
}
console.log(mostrarMayor(arreglo));

//Función para llenar con valores aleatorios el arreglo
function randomVar(arreglo2){
    let tamaño = arreglo2.length;
    for(let con=0; con < tamaño; con++){
        arreglo2[con] = (Math.random() * 10).toFixed(0);
        console.log(con + ":" + arreglo2[con]);
    }
}
console.log(randomVar(arreglo2));

//Función que muestra el valor menor y la posición del arreglo
function mostrarMenor(arreglo){
    let tamaño = arreglo.length;
    let menor = 0;
    for(let i = 0; i < tamaño; i++){
        if(arreglo[i] <= arreglo[i+1]){
            arreglo[i+1] = arreglo[i];
            menor = arreglo[i+1];
        }
    }
    console.log("Menor: " + menor);
}
console.log(mostrarMenor(arreglo));

function asimetrico(arreglo){
    let tamaño = arreglo.length;
    let pares = 0;
    let impares = 0;
    let diferencia = 0;
    let asimetrico ="";
    for(let i = 0; i< tamaño;i++){
        if(arreglo[i]%2 == 0){
            pares = pares + 1;
        }
        else{
            impares = impares + 1;
        }
    }
    pares = ((pares*100)/tamaño).toFixed(0);
    impares = ((impares*100)/tamaño).toFixed(0);
    if(pares>impares){
        diferencia = pares-impares;
    }
    else if(impares>pares){
        diferencia = impares-pares;
    }
    if(diferencia<=20){
        asimetrico="Es simetrico"
    }
    else{
        asimetrico="No es simetrico"
    }
    console.log("Porcentaje de Pares: "+pares);
    console.log("Porcentaje de impares: "+impares);
    console.log("El generador: "+asimetrico);
}
console.log(asimetrico(arreglo2));
//Funciones para la pagina
let porPares = document.getElementById("porPares");
let porImpares = document.getElementById("porImpares");
let EsSimetrico = document.getElementById("EsSimetrico");
function comprobarGenerador(){
    let x = document.getElementById("limite").value;
    let arreglo3 = new Array(x);
    randomVar2(arreglo3,x);
    mostrarArray2(arreglo3,x);
    asimetrico2(arreglo3,x);
}
function randomVar2(arreglo2,tamaño){
    for(let con=0; con < tamaño; con++){
        arreglo2[con] = (Math.random() * 50).toFixed(0);
    }
}

function mostrarArray2(arreglo,tamaño){
    let combox= document.getElementById("numeros");
    if(tamaño>0){
        for(let con=0; con < tamaño; ++ con){
            let y = document.createElement("option");
            y.text = arreglo[con];
            combox.remove(y);
        }
    }
    for(let con=0; con < tamaño; ++ con){
        let y = document.createElement("option");
        y.text = arreglo[con];
        combox.appendChild(y);
    }
}

function asimetrico2(arreglo,tamaño){
    let pares = 0;
    let impares = 0;
    let diferencia = 0;
    let asimetrico ="";
    for(let i = 0; i< tamaño;i++){
        if(arreglo[i]%2 == 0){
            pares = pares + 1;
        }
        else{
            impares = impares + 1;
        }
    }
    pares = ((pares*100)/tamaño).toFixed(0);
    impares = ((impares*100)/tamaño).toFixed(0);
    if(pares>impares){
        diferencia = pares-impares;
    }
    else if(impares>pares){
        diferencia = impares-pares;
    }
    if(diferencia<=20){
        asimetrico="Es simetrico"
    }
    else{
        asimetrico="No es simetrico"
    }
    porPares.textContent=pares;
    porImpares.textContent=impares;
    EsSimetrico.textContent=asimetrico;
}
//se termino todo