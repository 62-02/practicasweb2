  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
  import { getAnalytics } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-analytics.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyDu5m_xRjf7XLTE3BYlkDpZCj4ZT5NJ1Gk",
    authDomain: "proyecto-patrick.firebaseapp.com",
    projectId: "proyecto-patrick",
    storageBucket: "proyecto-patrick.appspot.com",
    messagingSenderId: "520991521036",
    appId: "1:520991521036:web:a153b4b03bad63ea0eb129",
    measurementId: "G-VYWMNNQYY0"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);